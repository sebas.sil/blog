package br.com.corcunda.blog.repository;

import br.com.corcunda.blog.model.Role;
import br.com.corcunda.blog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RoleRepository extends JpaRepository<Role /*model*/, Long /*id*/> {

}

package br.com.corcunda.blog.repository;

import br.com.corcunda.blog.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User /*model*/, Long /*id*/> {
    /**
     * Finds user by username as a search criteria.
     * @param username
     * @return  An user which username is an exact match (case sensitive) with the given username.
     *          If no user is found, this method returns null.
     */
    @Query("SELECT u FROM User u WHERE u.username = :username")
    User findByUsername(String username);
}

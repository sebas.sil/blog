package br.com.corcunda.blog.repository;

import br.com.corcunda.blog.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlogRepository extends JpaRepository<Post /*model*/, Long /*id*/> {
}

package br.com.corcunda.blog.service.impl;

import br.com.corcunda.blog.model.Post;
import br.com.corcunda.blog.model.User;
import br.com.corcunda.blog.repository.BlogRepository;
import br.com.corcunda.blog.repository.UserRepository;
import br.com.corcunda.blog.service.BlogService;
import br.com.corcunda.blog.service.UserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    public User findByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public User save(User user) {
        return repository.save(user);
    }
}

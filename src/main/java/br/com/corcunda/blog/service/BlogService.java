package br.com.corcunda.blog.service;

import br.com.corcunda.blog.model.Post;

import java.util.List;

public interface BlogService {
    List<Post> findAll();
    Post findById(Long id);
    Post save(Post post);
}

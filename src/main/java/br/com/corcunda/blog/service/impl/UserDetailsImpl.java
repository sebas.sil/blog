package br.com.corcunda.blog.service.impl;

import br.com.corcunda.blog.config.BlogUserDetails;
import br.com.corcunda.blog.model.User;
import br.com.corcunda.blog.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class UserDetailsImpl implements UserDetailsService {

    @Autowired
    UserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByUsername(username);

        if(user == null){
            throw new UsernameNotFoundException("User not found");
        }
        return new BlogUserDetails(user);
    }
}

package br.com.corcunda.blog.service;

import br.com.corcunda.blog.model.Post;
import br.com.corcunda.blog.model.User;

import java.util.List;

public interface UserService {
    User findByUsername(String username);
    User save(User user);
}

package br.com.corcunda.blog.controller;

import br.com.corcunda.blog.model.Post;
import br.com.corcunda.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BlogController {

    final BlogService service;

    public BlogController(BlogService service) {
        this.service = service;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ModelAndView getPosts(){
        ModelAndView mv = new ModelAndView("posts");
        List<Post> posts = service.findAll();
        mv.addObject("posts", posts);
        return mv;
    }

    @RequestMapping(value = "/posts/{id}", method = RequestMethod.GET)
    public ModelAndView getPost(@PathVariable("id") Long id){
        ModelAndView mv = new ModelAndView("post");
        Post post = service.findById(id);
        mv.addObject("post", post);
        return mv;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public String savePost(@Valid Post post, BindingResult result, RedirectAttributes attributes){
        System.out.println(post);
        System.out.println(result.getAllErrors().stream().map(a -> ((FieldError)a).getField()).collect(Collectors.joining(",")));
        if(result.hasErrors()){
            attributes.addFlashAttribute("message", "Verifique se os campos foram preenchidos");
            return "redirect:/newPost";
        }
        post.setDate(LocalDate.now());
        post = service.save(post);
        return new StringBuilder().append("redirect:/posts/").append(post.getId()).toString();
    }

    @RequestMapping(value = "/newPost", method = RequestMethod.GET)
    public String newPostForm(){
        return "postForm";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(){
        return new StringBuilder().append("redirect:/posts").toString();
    }
}

package br.com.corcunda.blog.model.seed;

import br.com.corcunda.blog.model.Role;
import br.com.corcunda.blog.model.User;
import br.com.corcunda.blog.repository.RoleRepository;
import br.com.corcunda.blog.repository.UserRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta classe cria roles ficticios quando o banco esta vazio
 */
@Component
public class RoleSeed {

    private final RoleRepository repository;

    public RoleSeed(RoleRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    private void createRoles(){
        if(repository.count() == 0){
            Role admin = new Role(1L, "ADMIN");
            Role user = new Role(2L, "USER");
            Role publisher = new Role(3L, "PUBLISHER");

            repository.save(admin);
            repository.save(user);
            repository.save(publisher);
        }
    }

}

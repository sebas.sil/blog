package br.com.corcunda.blog.model.seed;

import br.com.corcunda.blog.model.Role;
import br.com.corcunda.blog.model.User;
import br.com.corcunda.blog.repository.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Esta classe cria usuarios ficticios quando o banco esta vazio
 */
@Component
public class UserSeed {

    private final UserRepository repository;

    public UserSeed(UserRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    private void createUsers(){
        if(repository.count() == 0){
            List<Role> roles = new ArrayList<>();
            Role admin = new Role(1L, "ADMIN");
            Role publisher = new Role(3L, "PUBLISHER");

            String pass = new BCryptPasswordEncoder().encode("123");

            User u1 = new User(0L, "fabio", pass, true, LocalDate.now());
            u1.getRoles().add(admin);

            User u2 = new User(0L, "ricardo", pass, true, LocalDate.now());

            User u3 = new User(0L, "marcelo", pass, true, LocalDate.now());
            u3.getRoles().add(publisher);

            repository.save(u1);
            repository.save(u2);
            repository.save(u3);
        }
    }

}

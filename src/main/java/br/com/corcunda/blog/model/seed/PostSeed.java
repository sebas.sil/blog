package br.com.corcunda.blog.model.seed;

import br.com.corcunda.blog.model.Post;
import br.com.corcunda.blog.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

/**
 * Esta classe cria posts ficticios quando o banco esta vazio
 */
@Component
public class PostSeed {

    @Autowired
    private BlogRepository repository;

    @PostConstruct
    private void createPosts(){
        if(repository.count() == 0){
            Post p1 = new Post(0L, "Spring Boot #1", "Fábio Silveira", LocalDate.now(), "Criação do projeto");
            Post p2 = new Post(0L, "Spring Boot #2", "Fábio Silveira", LocalDate.now(), "Criação do banco");
            Post p3 = new Post(0L, "Spring Boot #3", "Fábio Silveira", LocalDate.now(), "Seeds");

            repository.save(p1);
            repository.save(p2);
            repository.save(p3);
        }
    }

}
